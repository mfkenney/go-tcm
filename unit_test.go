package tcm

import (
	"bytes"
	"io/ioutil"
	"reflect"
	"testing"
)

func TestCrc(t *testing.T) {
	tt := []struct {
		input    []byte
		expected uint16
	}{
		{[]byte("\x00\x05\x01"), 0xefd4},
	}

	for _, e := range tt {
		crc := CcittCrc(0)
		crc.Update(e.input)
		if uint16(crc) != e.expected {
			t.Errorf("Bad CRC, expected %x; got %x", e.expected, uint16(crc))
		}
	}
}

func TestReader(t *testing.T) {
	tt := []struct {
		input []byte
		count int
	}{
		{[]byte("\x00\x05\x01\xef\xd4"), 1},
	}

	for _, e := range tt {
		dr := NewReader(bytes.NewReader(e.input))
		b, err := ioutil.ReadAll(dr)
		if err != nil {
			t.Fatal(err)
		}
		if len(b) != e.count {
			t.Errorf("Wrong read count, expected %d; got %d", e.count, len(b))
		}
	}
}

func TestMountingRefNames(t *testing.T) {
	tt := []struct {
		name string
		val  MountingRef
	}{
		{name: "std0", val: Std0},
		{name: "STd0", val: Std0},
		{name: "xUP90", val: Xup90},
	}

	for _, e := range tt {
		val, err := MountingRefValue(e.name)
		if err != nil {
			t.Error(err)
		}
		if val != e.val {
			t.Errorf("Bad value; got %v, expected %v", val, e.val)
		}
	}

	_, err := MountingRefValue("foo")
	if err == nil {
		t.Errorf("Bad value name not caught")
	}
}

func TestComponentNames(t *testing.T) {
	tt := []struct {
		names []string
		vals  []ComponentId
	}{
		{names: []string{"Heading", "Pangle", "x"}, vals: []ComponentId{Heading, Pangle}},
		{names: []string{"magx", "magz"}, vals: []ComponentId{MagX, MagZ}},
	}

	for _, e := range tt {
		vals := ComponentIds(e.names...)
		if !reflect.DeepEqual(vals, e.vals) {
			t.Errorf("Mismatch; got %v, expected %v", vals, e.vals)
		}
	}
}

func TestFrameIo(t *testing.T) {
	var rw bytes.Buffer

	dev := NewTcm(&rw)
	mi := ModuleInfo{}
	copy(mi.Type[:], []byte("TCM6"))
	copy(mi.Revision[:], []byte("0042"))

	err := dev.SendFrame(Kmodinforesp, mi)
	if err != nil {
		t.Fatal(err)
	}

	resp := struct {
		Id   FrameType
		Data ModuleInfo
	}{}
	err = dev.RecvFrame(&resp)
	if err != nil {
		t.Fatal(err)
	}

	if resp.Data != mi {
		t.Errorf("Data mismatch; expected %#v, got %#v", mi, resp.Data)
	}
}

func TestDataDecode(t *testing.T) {
	var rw bytes.Buffer

	dev := NewTcm(&rw)
	frame := "\x05\x03\x05\x00\x00\x00\x00\x18\x00\x00\x00\x00\x19\x00\x00\x00\x00"

	cmpts := []ComponentId{Heading, Pangle, Rangle}
	expect := []DataValue{}
	for _, name := range cmpts {
		expect = append(expect, DataValue{Id: name, Data: float32(0)})
	}
	dev.SetDataComponents(cmpts...)
	rw.Reset()

	dev.wtr.Write([]byte(frame))

	vals, err := dev.dataResponse()
	if err != nil {
		t.Fatal(err)
	}

	if !reflect.DeepEqual(vals, expect) {
		t.Errorf("Decode failed; expected %#v, got %#v", expect, vals)
	}
}

func BenchmarkDataDecode(b *testing.B) {
	var rw bytes.Buffer

	dev := NewTcm(&rw)
	frame := "\x05\x03\x05\x00\x00\x00\x00\x18\x00\x00\x00\x00\x19\x00\x00\x00\x00"

	cmpts := []ComponentId{Heading, Pangle, Rangle}
	expect := []DataValue{}
	for _, name := range cmpts {
		expect = append(expect, DataValue{Id: name, Data: float32(0)})
	}
	dev.SetDataComponents(cmpts...)
	rw.Reset()
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		dev.wtr.Write([]byte(frame))
		dev.dataResponse()
	}
}
