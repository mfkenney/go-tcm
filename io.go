package tcm

import (
	"encoding/binary"
	"errors"
	"io"
)

var TraceFunc func(string, ...interface{})

// Writer writes PNI Binary Protocol datagrams to an underlying io.Writer
type Writer struct {
	buf []byte
	wtr io.Writer
}

// NewWriter returns a new Writer wrapping an io.Writer
func NewWriter(w io.Writer) *Writer {
	return &Writer{wtr: w}
}

// Write sends the contents of p as a single datagram and returns any
// error that occurs
func (w *Writer) Write(p []byte) (int, error) {
	crc := CcittCrc(0)
	count := uint16(len(p) + 4)
	crc.Update([]byte{uint8(count >> 8), uint8(count & 0xff)})
	crc.Update(p)
	if TraceFunc != nil {
		TraceFunc("OUT: %d %v %04x", count, p, uint16(crc))
	}
	binary.Write(w.wtr, ByteOrder, count)
	n, _ := w.wtr.Write(p)
	err := binary.Write(w.wtr, ByteOrder, uint16(crc))
	return n, err
}

// Reader reads PNI Binary Protocol datagrams from an underlying io.Reader.
type Reader struct {
	buf  []byte
	rd   io.Reader
	r, w int
	crc  CcittCrc
}

func NewReader(rd io.Reader) *Reader {
	return &Reader{rd: rd}
}

func (dr *Reader) fill_buf() error {
	var (
		count, crc uint16
		buf        [2]byte
	)

	dr.crc = CcittCrc(0)
	n, err := dr.rd.Read(buf[:])
	if err != nil {
		return err
	}
	if n != len(buf) {
		return errors.New("short read")
	}
	count = ByteOrder.Uint16(buf[:2])

	dr.crc.Update(buf[:2])
	dr.buf = make([]byte, count-4)
	dr.r = 0
	dr.w = 0
	n, err = io.ReadFull(dr.rd, dr.buf)
	if err != nil {
		return err
	}
	dr.w += n
	dr.crc.Update(dr.buf)
	err = binary.Read(dr.rd, ByteOrder, &crc)
	if err != nil {
		return err
	}
	if TraceFunc != nil {
		TraceFunc("IN: %d %v %04x", count, dr.buf, crc)
	}
	if uint16(dr.crc) != crc {
		return errors.New("CRC mismatch")
	}

	return nil
}

// Read reads (at most) the contents of a single TCM datagram from the
// underlying io.Reader.
func (dr *Reader) Read(p []byte) (int, error) {
	n := len(p)
	if n == 0 {
		return 0, nil
	}

	if dr.r == dr.w {
		err := dr.fill_buf()
		if err != nil {
			return 0, err
		}
	}

	n = copy(p, dr.buf[dr.r:dr.w])
	dr.r += n
	return n, nil
}
