// Package tcm provides functions and data structures for accessing PNI
// tilt-compensated compass (TCM) sensors.
package tcm

import (
	"bytes"
	"context"
	"encoding/binary"
	"errors"
	"fmt"
	"io"
	"math"
	"reflect"
	"time"
)

const syncModeDelay time.Duration = time.Millisecond

// Byte order for all binary messages
var ByteOrder binary.ByteOrder = binary.BigEndian

// Data components for SyncRead
var syncModeComponents = []ComponentId{Heading, Pangle, Rangle}

// Errors
var (
	InvalidResponse = errors.New("Invalid response")
)

type ModuleInfo struct {
	Type, Revision [4]byte
}

func (mi ModuleInfo) String() string {
	return fmt.Sprintf("%s:%s", mi.Type, mi.Revision)
}

// DataValue represents a single component of a data record.
type DataValue struct {
	Id   ComponentId
	Data interface{}
}

// Device represents a generic PNI device
type Device struct {
	rdr        *Reader
	wtr        *Writer
	components []ComponentId
}

func newDevice(port io.ReadWriter) *Device {
	return &Device{
		rdr: NewReader(port),
		wtr: NewWriter(port),
	}
}

// Tcm represents a PNI Tilt-compensated Compass module
type Tcm struct {
	*Device
	mode ModeId
}

// NewTcm creates a new Tcm at the specified serial port.
func NewTcm(port io.ReadWriter) *Tcm {
	t := &Tcm{}
	t.Device = newDevice(port)
	return t
}

// Trax represents a PNI TRAX2 AHRS and digital compass
type Trax struct {
	*Device
}

// NewTrax creates a new Trax at the specified serial port.
func NewTrax(port io.ReadWriter) *Trax {
	t := &Trax{}
	t.Device = newDevice(port)
	return t
}

// dataSize returns the size of the response to the GetData command. The size
// is dependent on the data components selected by the user.
func dataSize(cpts []ComponentId) int {
	n := len(cpts)
	if n == 0 {
		return 0
	}

	for _, c := range cpts {
		switch c {
		case Distortion, CalStatus, HeadingStatus:
			n += 1
		default:
			n += 4
		}
	}

	return n
}

// configSize returns the size of the response to the GetConfig command
func configSize(id ConfigId) int {
	n := int(1)
	switch id {
	case Kdeclination, Kusercalnumpoints:
		n += 4
	default:
		n += 1
	}
	return n
}

// SendFrame sends a single data frame to the TCM device
func (d *Device) SendFrame(id FrameType, payload interface{}) error {
	var buf bytes.Buffer

	buf.Write([]byte{uint8(id)})
	if payload != nil {
		err := binary.Write(&buf, ByteOrder, payload)
		if err != nil {
			return err
		}
	}

	_, err := d.wtr.Write(buf.Bytes())
	return err
}

// RecvFrame receives a single data frame from from the TCM device.
func (d *Device) RecvFrame(v interface{}) error {
	rv := reflect.ValueOf(v)
	if rv.Kind() != reflect.Ptr || rv.IsNil() {
		return errors.New("Need a pointer to Frame contents")
	}

	n := binary.Size(v)
	if n <= 0 {
		return errors.New("Bad data frame size")
	}
	return binary.Read(d.rdr, ByteOrder, v)
}

// GetModinfo returns the TCM device's type and firmware revision.
func (d *Device) GetModInfo() (ModuleInfo, error) {
	var req interface{}
	resp := struct {
		Id   FrameType
		Data ModuleInfo
	}{}

	err := d.SendFrame(Kgetmodinfo, req)
	if err != nil {
		return resp.Data, err
	}
	err = d.RecvFrame(&resp)
	return resp.Data, err
}

// SetDataComponents specifies the components to include in the GetData response.
func (d *Device) SetDataComponents(ids ...ComponentId) error {
	req := make([]uint8, 0, len(ids)+1)
	req = append(req, uint8(len(ids)))
	for _, id := range ids {
		req = append(req, uint8(id))
	}

	err := d.SendFrame(Ksetdatacomponents, req)
	if err != nil {
		return err
	}
	d.components = ids
	return nil
}

// SetConfig sets an internal configuration value
func (d *Device) SetConfig(id ConfigId, value interface{}) error {
	var buf bytes.Buffer
	err := binary.Write(&buf, ByteOrder, id)

	switch id {
	case Kdeclination:
		err = binary.Write(&buf, ByteOrder, float32(value.(float64)))
	case Kusercalnumpoints:
		err = binary.Write(&buf, ByteOrder, uint32(value.(int)))
	case Ktruenorth, Kbigendian, Kusercalautosampling:
		err = binary.Write(&buf, ByteOrder, value.(bool))
	case Kmountingref:
		err = binary.Write(&buf, ByteOrder, value.(MountingRef))
	default:
		err = binary.Write(&buf, ByteOrder, uint8(value.(int)))
	}

	if err != nil {
		return err
	}

	err = d.SendFrame(Ksetconfig, buf.Bytes())

	if err != nil {
		return err
	}
	var resp FrameType
	err = d.RecvFrame(&resp)
	if err != nil {
		return err
	}

	if resp != Ksetconfigdone {
		return fmt.Errorf("%#v: %w", resp, InvalidResponse)
	}

	return nil
}

// GetConfig returns the value of an internal configuration value
func (d *Device) GetConfig(id ConfigId) (interface{}, error) {
	var val interface{}

	req := struct {
		Id ConfigId
	}{Id: id}

	err := d.SendFrame(Kgetconfig, req)
	if err != nil {
		return val, err
	}

	var (
		buf  bytes.Buffer
		bval bool
		cval uint8
		fval float32
		ival uint32
	)

	n := configSize(id)
	_, err = io.CopyN(&buf, d.rdr, int64(n)+1)
	if err != nil {
		return val, err
	}

	binary.Read(&buf, ByteOrder, &cval)
	if FrameType(cval) != Kconfigresp {
		return val, fmt.Errorf("%v: %w", cval, InvalidResponse)
	}

	binary.Read(&buf, ByteOrder, &cval)
	switch c := ConfigId(cval); c {
	case Kdeclination:
		binary.Read(&buf, ByteOrder, &fval)
		val = fval
	case Kusercalnumpoints:
		binary.Read(&buf, ByteOrder, &ival)
		val = ival
	case Ktruenorth, Kbigendian, Kusercalautosampling:
		binary.Read(&buf, ByteOrder, &bval)
		val = bval
	case Kmountingref:
		binary.Read(&buf, ByteOrder, &cval)
		val = MountingRef(cval)
	default:
		binary.Read(&buf, ByteOrder, &cval)
		val = cval
	}

	return val, nil
}

// Save stores the current device configuration in non-volatile memory.
func (d *Device) Save() error {
	var req interface{}
	resp := struct {
		Id   FrameType
		Data uint16
	}{}

	err := d.SendFrame(Ksave, req)
	if err != nil {
		return err
	}
	err = d.RecvFrame(&resp)
	if err != nil {
		return err
	}

	if resp.Id != Ksavedone {
		return fmt.Errorf("%#v: %w", resp, InvalidResponse)
	}

	if resp.Data != 0 {
		return errors.New("kSave error")
	}

	return nil
}

func (d *Device) dataResponse() ([]DataValue, error) {
	// Read the entire frame into a buffer
	n := dataSize(d.components) + 2
	buf := make([]byte, n)
	_, err := d.rdr.Read(buf)
	if err != nil {
		return nil, err
	}

	if frame_id := FrameType(buf[0]); frame_id != Kgetdataresp {
		return nil, fmt.Errorf("%v: %w", frame_id, InvalidResponse)
	}
	count := buf[1]
	resp := make([]DataValue, 0, count)

	var fval float32
	offset := 2
	for offset < n {
		switch id := ComponentId(buf[offset]); id {
		case Distortion, CalStatus, HeadingStatus:
			resp = append(resp, DataValue{Id: id, Data: buf[offset+1]})
			offset += 2
		default:
			fval = math.Float32frombits(ByteOrder.Uint32(buf[offset+1:]))
			resp = append(resp, DataValue{Id: id, Data: fval})
			offset += 5
		}
	}

	return resp, nil
}

// GetData returns a single measurement data set.
func (d *Device) GetData() ([]DataValue, error) {
	var req interface{}

	n := dataSize(d.components)
	if n == 0 {
		return nil, errors.New("No data components selected")
	}

	err := d.SendFrame(Kgetdata, req)
	if err != nil {
		return nil, err
	}

	return d.dataResponse()
}

// StreamData returns a channel which will produce data measurements
// continuously until the associated Context is cancelled.
func (d *Device) StreamData(ctx context.Context) <-chan []DataValue {
	ch, _ := d.StreamDataWithErr(ctx)
	return ch
}

// Like StreamData but returns a second channel to receive any error that occurs.
func (d *Device) StreamDataWithErr(ctx context.Context) (<-chan []DataValue, <-chan error) {
	ch := make(chan []DataValue, 1)
	err_ch := make(chan error, 1)

	go func() {
		defer close(ch)
		defer close(err_ch)
		var req interface{}

		err := d.SendFrame(Kstartcontinuousmode, req)
		if err != nil {
			err_ch <- fmt.Errorf("Kstartcontinuousmode: %w", err)
			return
		}

	loop:
		for {
			rec, err := d.dataResponse()
			if err != nil {
				err_ch <- fmt.Errorf("Kgetdataresp: %w", err)
				break
			}
			ch <- rec

			select {
			case <-ctx.Done():
				break loop
			default:
			}
		}

		err = d.SendFrame(Kstopcontinuousmode, req)
		if err != nil {
			err_ch <- fmt.Errorf("Kstopcontinuousmode: %w", err)
		}
	}()

	return ch, err_ch
}

// SetAcqParams sets the data acquisition parameters for the device.
func (d *Device) SetAcqParams(mode AcquistionMode, flush bool,
	acqDelay, sampDelay time.Duration) error {
	ap := struct {
		Mode                uint8
		Flush               bool
		AcqDelay, SampDelay float32
	}{
		Mode:      uint8(mode),
		Flush:     flush,
		AcqDelay:  float32(acqDelay.Seconds()),
		SampDelay: float32(sampDelay.Seconds()),
	}

	err := d.SendFrame(Ksetacqparams, ap)
	if err != nil {
		return err
	}
	var resp FrameType
	err = d.RecvFrame(&resp)
	if err != nil {
		return err
	}
	if resp != Kacqparamsdone {
		return fmt.Errorf("%#v: %w", resp, InvalidResponse)
	}

	return nil
}

func (d *Device) setFilterCoeffs(vals []float64) error {
	fv := struct {
		Val1, Val2, Count uint8
		Coeffs            []float64
	}{
		Val1:   3,
		Val2:   1,
		Count:  uint8(len(vals)),
		Coeffs: vals,
	}
	err := d.SendFrame(Ksetfirfilters, fv)
	if err != nil {
		return err
	}

	var resp FrameType
	err = d.RecvFrame(&resp)
	if err != nil {
		return err
	}
	if resp != Ksetfirfiltersdone {
		return fmt.Errorf("%#v: %w", resp, InvalidResponse)
	}

	return nil
}

func (d *Device) SetNTapFilter(vals []float64) error {
	switch len(vals) {
	case 4, 8, 16, 32:
		return d.setFilterCoeffs(vals)
	}
	return fmt.Errorf("Invalid filter length: %d", len(vals))
}

// PowerDown puts the device in sleep mode
func (d *Device) PowerDown() error {
	var resp FrameType
	err := d.SendFrame(Kpowerdown, nil)
	if err != nil {
		return err
	}
	err = d.RecvFrame(&resp)
	if resp != Kpowerdowndone {
		return fmt.Errorf("%#v: %w", resp, InvalidResponse)
	}
	return nil
}

func (d *Device) wakeup() (err error) {
	_, err = d.wtr.wtr.Write([]byte{0xff})
	time.Sleep(syncModeDelay)
	return err
}

// PowerUp takes the device out of sleep mode.
func (d *Device) PowerUp() error {
	d.wakeup()

	var resp FrameType
	err := d.RecvFrame(&resp)
	if err != nil {
		return err
	}
	if resp != Kpowerupdone {
		return fmt.Errorf("%#v: %w", resp, InvalidResponse)
	}
	return nil
}

// SyncMode puts the device in Sync Mode (see section 7.3.34 of the manual)
func (t *Tcm) SyncMode() (err error) {
	if t.mode == Sync {
		return nil
	}

	req := struct {
		Mode ModeId
	}{Mode: Sync}

	resp := struct {
		Id   FrameType
		Mode ModeId
	}{}

	err = t.SendFrame(Ksetmode, req)
	if err != nil {
		return err
	}
	err = t.RecvFrame(&resp)
	if err != nil {
		return err
	}
	if resp.Mode != Sync {
		return fmt.Errorf("%#v: %w", resp, InvalidResponse)
	}

	t.mode = Sync
	t.components = syncModeComponents
	// A data frame is sent as part of the acknowledgement, ignore it.
	_, err = t.dataResponse()
	return err
}

// NormalMode takes the device out of Sync Mode.
func (t *Tcm) NormalMode() (err error) {
	if t.mode == Normal {
		return nil
	}
	t.wakeup()

	req := struct {
		Mode ModeId
	}{Mode: Normal}

	resp := struct {
		Id   FrameType
		Mode ModeId
	}{}

	err = t.SendFrame(Ksetmode, req)
	if err != nil {
		return err
	}
	err = t.RecvFrame(&resp)
	if err != nil {
		return err
	}
	if resp.Mode != Normal {
		return fmt.Errorf("%#v: %w", resp, InvalidResponse)
	}

	t.mode = Normal
	return nil
}

// SyncRead is used to read a data frame while the device is in Sync
// Mode. The data frame components are always Heading, Pangle, and Rangle.
func (t *Tcm) SyncRead() ([]DataValue, error) {
	if t.mode == Normal {
		return nil, fmt.Errorf("TCM is not in Sync Mode")
	}
	var req interface{}

	t.wakeup()
	err := t.SendFrame(Ksyncread, req)
	if err != nil {
		return nil, err
	}

	return t.dataResponse()
}

// Mode gets the current functional mode of the Trax device
func (t *Trax) Mode() (FunctionalMode, error) {
	resp := struct {
		Id   FrameType
		Mode FunctionalMode
	}{Mode: UnknownMode}

	err := t.SendFrame(Kgetfuncmode, nil)
	if err != nil {
		return resp.Mode, err
	}

	err = t.RecvFrame(&resp)
	if resp.Id != Kfuncmoderesp {
		err = fmt.Errorf("%#v: %w", resp, InvalidResponse)
	}
	return resp.Mode, err
}

// SetMode sets the functional mode of the Trax device.
func (t *Trax) SetMode(mode FunctionalMode) error {
	req := struct {
		Mode FunctionalMode
	}{Mode: mode}

	return t.SendFrame(Ksetfuncmode, req)
}

// SetMagRef establishes the criteria for a  distortion-free local magnetic
// field. It should be sent before operating the device in AHRS mode. See
// section 7.5.1 of the TRAX User Manual.
func (t *Trax) SetMagRef() error {
	return t.SendFrame(KsetResetRef, nil)
}

// SetMergeRate sets the time constant which determines how quickly the
// TRAX2 algorithm converges to the sensor readings. The allowable range is
// 100ms to 20m.
func (t *Trax) SetMergeRate(settle time.Duration) error {
	tc := math.Sqrt(settle.Seconds() / 3)
	if tc < 0.1 || tc > 20 {
		return fmt.Errorf("Settle time out of range: %s", settle)
	}
	req := struct {
		RateId uint8
		Rate   float32
	}{}

	// Combined magenetic and accelerometer merge rate
	req.RateId = 5
	req.Rate = float32(tc)
	t.SendFrame(Ksetmergerate, req)

	// Magnetic merge rate
	req.RateId = 6
	return t.SendFrame(Ksetmergerate, req)
}
