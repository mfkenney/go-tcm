package tcm

import (
	"errors"
	"strings"
)

type FrameType uint8

const (
	Kgetmodinfo FrameType = iota + 1
	Kmodinforesp
	Ksetdatacomponents
	Kgetdata
	Kgetdataresp
	Ksetconfig
	Kgetconfig
	Kconfigresp
	Ksave
	Kstartcal
	Kstopcal
	Ksetfirfilters
	Kgetfirfilters
	Kgetfirfiltersresp
	Kpowerdown
	Ksavedone
	Kusercalsampcount
	Kusercalscore
	Ksetconfigdone
	Ksetfirfiltersdone
	Kstartcontinuousmode
	Kstopcontinuousmode
	Kpowerupdone
	Ksetacqparams
	Kgetacqparams
	Kacqparamsdone
	Kacqparamsresp
	Kpowerdowndone
	Kfactoryusercal
	Kfactoryusercaldone
	Ktakeusercalsample
	Kfactoryinclcal     = 36
	Kfactoryinclcaldone = 37
	// TCM only
	Ksetmode     = 46
	Ksetmoderesp = 47
	Ksyncread    = 49
	// TRAX only
	Kserialnumber    = 52
	Ksnresponse      = 53
	Ksetfuncmode     = 79
	Kgetfuncmode     = 80
	Kfuncmoderesp    = 81
	Ksetdistortmode  = 107
	Kgetdistortmode  = 108
	Kdistortmoderesp = 109
	KsetResetRef     = 110
	Ksetmergerate    = 128
	Kgetmergerate    = 129
	Kmergerateresp   = 130
)

//go:generate stringer -type=FrameType
var frameTypeTable map[string]FrameType

func init() {
	frameTypeTable = make(map[string]FrameType)
	for val := Kgetmodinfo; val <= Kmergerateresp; val++ {
		if s := val.String(); !strings.HasPrefix(s, "FrameType(") {
			frameTypeTable[strings.ToLower(s)] = val
		}
	}
}

type ComponentId uint8

const (
	Heading       ComponentId = 5
	Temperature   ComponentId = 7
	Distortion    ComponentId = 8
	CalStatus     ComponentId = 9
	AccelX        ComponentId = 21
	AccelY        ComponentId = 22
	AccelZ        ComponentId = 23
	Pangle        ComponentId = 24
	Rangle        ComponentId = 25
	MagX          ComponentId = 27
	MagY          ComponentId = 28
	MagZ          ComponentId = 29
	GyroX         ComponentId = 74
	GyroY         ComponentId = 75
	GyroZ         ComponentId = 76
	HeadingStatus ComponentId = 79
)

//go:generate stringer -type=ComponentId
var componentIdTable map[string]ComponentId

func init() {
	componentIdTable = make(map[string]ComponentId)
	for val := Heading; val <= HeadingStatus; val++ {
		if s := val.String(); !strings.HasPrefix(s, "ComponentId(") {
			componentIdTable[strings.ToLower(s)] = val
		}
	}
}

// Return a list of ComponentId values from a list of names
func ComponentIds(names ...string) []ComponentId {
	ids := make([]ComponentId, 0, len(names))
	for _, name := range names {
		val, ok := componentIdTable[strings.ToLower(name)]
		if ok {
			ids = append(ids, val)
		}
	}
	return ids
}

// IsValidComponent returns whether name represents a valid component ID
func IsValidComponent(name string) bool {
	_, ok := componentIdTable[strings.ToLower(name)]
	return ok
}

type ConfigId uint8

const (
	Kdeclination         ConfigId = 1
	Ktruenorth                    = 2
	Kbigendian                    = 6
	Kmountingref                  = 10
	Kusercalnumpoints             = 12
	Kusercalautosampling          = 13
	Kbaudrate                     = 14
)

type MountingRef uint8

const (
	Std0 MountingRef = iota + 1
	Xup0
	Yup0
	Std90
	Std180
	Std270
	Zdown0
	Xup90
	Xup180
	Xup270
	Yup90
	Yup180
	Yup270
	Zdown90
	Zdown180
	Zdown270
)

//go:generate stringer -type=MountingRef
var mountingRefTable map[string]MountingRef

func init() {
	mountingRefTable = make(map[string]MountingRef)
	for val := Std0; val <= Zdown270; val++ {
		mountingRefTable[strings.ToLower(val.String())] = val
	}
}

// Return a MountingRef value from its string representation
func MountingRefValue(s string) (MountingRef, error) {
	val, ok := mountingRefTable[strings.ToLower(s)]
	if !ok {
		return 0, errors.New("Invalid value")
	}
	return val, nil
}

type BaudRate uint8

const (
	B300    BaudRate = 0
	B600             = 1
	B1200            = 2
	B1800            = 3
	B2400            = 4
	B3600            = 5
	B4800            = 6
	B7200            = 7
	B9600            = 8
	B14400           = 9
	B19200           = 10
	B28800           = 11
	B38400           = 12
	B57600           = 13
	B115200          = 14
)

type CalType uint8

const (
	CalFullRange CalType = 10
	Cal2d                = 20
	CalHi                = 30
	CalLimitTilt         = 40
	CalAccelOnly         = 100
	CalAccelMag          = 110
)

type AxisId uint8

const (
	Kxaxis AxisId = iota + 1
	Kyaxis
	Kzaxis
)

//go:generate stringer -type=AxisId

type ModeId uint8

const (
	Normal ModeId = 0
	Sync          = 100
)

type AcquistionMode uint8

const (
	Polled     AcquistionMode = 0
	Continuous                = 1
)

type FunctionalMode uint8

const (
	CompassMode FunctionalMode = 0
	AHRSMode                   = 1
	UnknownMode                = 255
)
